# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

FLASK_ADMIN_SWATCH = "paper"
DATABASE_FILE = "ssh-data/ssh.sqlite"
SECRET_KEY = "sfadfasdhf29h237hf7ash71231sas5f871823"
SQLALCHEMY_DATABASE_URI = "sqlite:///../" + DATABASE_FILE
SQLALCHEMY_ECHO = True
SQLALCHEMY_TRACK_MODIFICATIONS = False
