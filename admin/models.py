# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from admin import db

AVAILABLE_KEYTYPES = [
    ("ssh-dss", "ssh-dss"),
    ("ssh-rsa", "ssh-rsa"),
    ("ssh-ed25519", "ssh-ed25519"),
    ("ecdsa-sha2-nistp256", "ecdsa-sha2-nistp256"),
]

at_key_profile = db.Table(
    "key_profile",
    db.Model.metadata,
    db.Column("key_id", db.Integer, db.ForeignKey("key.id")),
    db.Column("profile_id", db.Integer, db.ForeignKey("profile.id")),
)

at_profile_system = db.Table(
    "profile_system",
    db.Model.metadata,
    db.Column("profile_id", db.Integer, db.ForeignKey("profile.id")),
    db.Column("system_id", db.Integer, db.ForeignKey("system.id")),
)


class Key(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    keytype = db.Column(db.String(100))
    key = db.Column(db.String(2000))
    option = db.Column(db.String(250))
    name = db.Column(db.String(100))
    extra = db.Column(db.String(100))
    profiles = db.relationship(
        "Profile", secondary=at_key_profile, back_populates="keys"
    )
    comment = db.Column(db.String(100))

    def __init__(self):
        self.extra = "default"

    def __str__(self):
        if "default" not in self.extra:
            return "{}_{}".format(self.name, self.extra)
        else:
            return "{}".format(self.name)

    def __repr__(self):
        return "{}: {}".format(self.id, self.__str__())


class Profile(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    keys = db.relationship("Key", secondary=at_key_profile, back_populates="profiles")
    systems = db.relationship(
        "System", secondary=at_profile_system, back_populates="profiles"
    )

    def __repr__(self):
        return "{}".format(self.name)


class System(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.String(100))
    host = db.Column(db.String(100))
    authfile = db.Column(db.String(100))
    profiles = db.relationship(
        "Profile", secondary=at_profile_system, back_populates="systems"
    )

    def __str__(self):
        return "{}@{}".format(self.user, self.host)

    def __repr__(self):
        return "{}: {}".format(self.id, self.__str__())
