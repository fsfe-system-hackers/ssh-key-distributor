# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import random

from admin import db
from admin.models import Key, Profile, System


def build_sample_db():

    db.drop_all()
    db.create_all()

    keystrings = [
        "LinusTowerAAAAC3NzaC1lZDI1NTE5AAAAINkyEjE9OKTISvALPX8vYJozlmdKRbLZKPJjhKHNEZEJ",
        "MaxLaptopAAAAC3NzaC1lZDI1NTE5AAAAIF7NCeNnSRedoy32k63S61kZTDsf4Q7RQ5W7iBEoWGYM",
        "AlbertTowerAAAAC3NzaC1lZDI1NTE5AAAAILD4tEAlmgbRwoIaItooPeDR8oPlxPiD4/I0tpKYgYmq",
    ]

    keyoptions = [
        None,
        '''command="echo '...working'"''',
        None,
    ]

    keycomments = [
        "linus@tower",
        "max@laptop",
        "albert@tower",
    ]

    # Create sample key
    key_list = []
    for i in range(len(keystrings)):
        key = Key()
        key.keytype = "ed25519"
        key.option = keyoptions[i]
        key.comment = keycomments[i]
        key.key = keystrings[i]
        key_list.append(key)
        db.session.add(key)

    systemhosts = [
        "meitner.fsfeurope.org",
    ]

    systemusers = [
        "test",
        "docker",
    ]

    systemauthfiles = [
        "/root/.ssh/authorized_keys",
        "/home/docker/.ssh/authorized_keys",
        "/home/monitor/.ssh/authorized_keys2",
    ]

    system_list = []
    for i in range(len(systemhosts)):
        for j in range(len(systemusers)):
            system = System()
            system.host = systemhosts[i]
            system.authfile = systemauthfiles[j]
            system.user = systemusers[j]
            system_list.append(system)
            db.session.add(system)

    db.session.commit()
    return
