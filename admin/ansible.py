# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import collections
import os
import os.path as op
import shutil

from sqlalchemy import create_engine
from sqlalchemy.sql import select

from admin.config import DATABASE_FILE
from admin.models import Key, System, at_key_profile, at_profile_system

# Declare file locations
cwd = os.getcwd()
path_to_db = op.join(cwd, DATABASE_FILE)
path_to_keystringfiles = op.join(cwd, "ssh-data/keystringfiles")
path_to_hostvars = op.join(cwd, "host_vars")


def generate_config():
    """Generate content for ../host_vars and ../keystringfiles"""

    # Connect to database
    engine = create_engine("sqlite:///" + path_to_db, echo=True)
    conn = engine.connect()

    # Delete old files
    try:
        shutil.rmtree(path_to_keystringfiles)
        shutil.rmtree(path_to_hostvars)
    except:
        pass

    # Create the directories anew
    os.mkdir(path_to_keystringfiles)
    os.mkdir(path_to_hostvars)

    # Query database for keys on systems
    query_for_keys = (
        select(["*"])
        .select_from(
            at_key_profile.join(
                at_profile_system,
                at_key_profile.c.profile_id == at_profile_system.c.profile_id,
            )
            .join(Key, at_key_profile.c.key_id == Key.id)
            .join(System, at_profile_system.c.system_id == System.id)
        )
        .order_by(System.host)
    )

    system_key_rows = conn.execute(query_for_keys)

    # Transform queried data into Python-native data structures
    system_keys = collections.defaultdict(list)
    system_authfile = dict()
    hosts = list()
    for row in system_key_rows:
        host = row["host"]
        authfile = row["authfile"]
        system = row["user"] + "@" + host
        option = row["option"] + " " if row["option"] is not None else ""
        keystring = (
            option
            + row["keytype"]
            + " "
            + row["key"]
            + " "
            + row["name"]
            + "_"
            + row["extra"]
        )
        system_keys[system].append(keystring)
        system_authfile[system] = authfile
        hosts.append(host)
    hosts = list(set(hosts))

    # Write keys to keystringfiles/<system_name>.pub
    system_keystringfile = dict()
    for system, keys in system_keys.items():
        abs_path = path_to_keystringfiles + "/" + system + ".pub"
        rel_path = "keystringfiles/" + system + ".pub"
        with open(abs_path, "w+") as f:
            # pylint: disable=line-too-long
            f.write(
                "#####\n# ATTENTION: This file is managed by ssh-key-distributor. Do not update manually!\n#####\n"
            )
            for key in sorted(set(keys), key=lambda e: e.split()[-1]):
                f.write(key + "\n")
        system_keystringfile[system] = rel_path

    # Write host configuration to host_vars/<host_name>
    for host in hosts:
        with open(path_to_hostvars + "/" + host + ".yml", "w+") as f:
            f.write("---\nsystems:\n  ")
            for system, keys in system_keys.items():
                if host not in system:
                    continue
                f.write(system + ":\n    ")
                f.write("user: " + system.split("@")[0] + "\n    ")
                f.write("authfile: " + system_authfile[system] + "\n    ")
                f.write(
                    "keystringfile: "
                    + "ssh-data/"
                    + system_keystringfile[system]
                    + "\n\n  "
                )

    return "Sucessfully generated ansible config"
