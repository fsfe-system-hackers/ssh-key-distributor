# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import flask_admin as admin
from flask import Markup, redirect, render_template, send_file
from flask_admin.contrib import sqla

from admin import app, db
from admin.ansible import generate_config
from admin.models import AVAILABLE_KEYTYPES, Key, Profile, System


def key_formatter(view, context, model, name):
    return "..." + model.key[-10:] if model.key else None


@app.route("/favicon.ico")
def favicon():
    """Show favicon"""
    return send_file("static/favicon.ico")


@app.route("/")
def redirect_to_admin():
    """Redirect every request to root to /admin"""
    return redirect("/admin")


@app.route("/generate_ansible_config")
def generate_ansible_config():
    generate_config()
    return redirect("/admin")


class KeyAdmin(sqla.ModelView):
    form_args = dict(extra=dict(default="default"))
    page_size = 100
    can_export = False
    form_choices = {
        "keytype": AVAILABLE_KEYTYPES,
    }
    column_list = [
        "profiles",
        "name",
        "extra",
        "key",
        "option",
        "keytype",
        "comment",
    ]
    column_labels = {
        "keytype": "Key Type",
        "key": "Keystring",
        "name": "Username",
        "extra": "Extra",
        "profiles": "Profiles",
        # TODO legacy, TBR when Max has checked everything
        "option": "Option",
        "comment": "Comment",
    }
    column_searchable_list = [
        "key",
        "name",
        "extra",
        # TODO legacy, TBR when Max has checked everything
        "option",
        "comment",
    ]
    column_editable_list = [
        "name",
        "extra",
        # TODO legacy, TBR when Max has checked everything
        "option",
    ]
    column_formatters = {"key": key_formatter}


class ProfileAdmin(sqla.ModelView):
    can_export = True
    column_list = ["name", "systems", "keys"]


class SystemAdmin(sqla.ModelView):
    form_args = dict(
        authfile=dict(default="/root/.ssh/authorized_keys"),
        user=dict(default="root"),
    )
    page_size = 100
    column_list = [
        "user",
        "host",
        "authfile",
        "profiles",
    ]


admin = admin.Admin(app, name="SSH Key Distributor", template_mode="bootstrap3")

# Add views
admin.add_view(SystemAdmin(System, db.session, "Systems"))
admin.add_view(ProfileAdmin(Profile, db.session, "Profiles"))
admin.add_view(KeyAdmin(Key, db.session, "Keys"))
