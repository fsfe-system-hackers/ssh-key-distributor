# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import itertools
import random
import re

from admin import db
from admin.models import Key, Profile, System


def clean_db():
    """Drops and recreates all tables (empty) in database specified in admin/config.py"""
    db.drop_all()
    db.create_all()


def import_keys_from_file(path_to_file: str):
    """Import keys from file at <path_to_file>"""
    keytypes = []
    keystrings = []
    keyoptions = []
    keycomments = []

    # Parse the contents of the file
    keytype_matcher = re.compile(r"[ssh|ecdsa]*-\S*")
    keystring_matcher = re.compile(r"[ssh|ecdsa]*-\S*\s(\S*)")
    keycomment_matcher = re.compile(r"[ssh|ecdsa]*-\S*\s\S*\s(.*)")
    with open(path_to_file, "r") as f:
        for line in f:
            print(keycomment_matcher.search(line).group(1))
            keyoptions.append(None)
            keytypes.append(keytype_matcher.search(line).group(0))
            keystrings.append(keystring_matcher.search(line).group(1))
            keycomments.append(keycomment_matcher.search(line).group(1))

    # Write keys to the database
    for (keyoption, keytype, keystring, keycomment) in zip(
        keyoptions, keytypes, keystrings, keycomments
    ):
        key = Key()
        key.keytype = keytype
        key.option = keyoption
        key.key = keystring
        key.comment = keycomment
        db.session.add(key)

    db.session.commit()
    return "Wrote {} keys to database".format(len(keyoptions))


def import_systems_from_file(path_to_file: str):
    """Import sysmtes from file at <path_to_file>"""
    with open(path_to_file, "r") as file:
        users = []
        hosts = []
        authfiles = []
        for line in file:
            # Match only relevant lines
            if re.match(r"^-\s\S*\s\W.*", line):
                hosts.append(re.search(r"- (\S*)", line).group(1))
                users.append("root")
                authfiles.append("/root/.ssh/authorized_keys")
        for (user, host, authfile) in zip(users, hosts, authfiles):
            system = System()
            system.user = user
            system.host = host
            system.authfile = authfile
            db.session.add(system)

    db.session.commit()
    return "Wrote {} systems to database".format(len(users))


# clean_db()
# import_keys_from_file("ssh-data/raw/ssh-keys.txt")
# import_systems_from_file("inventory/inventory.txt")
