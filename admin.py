# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import os
import os.path as op

from admin import app
from admin.data import build_sample_db

# Build a sample db on the fly, if one does not exist yet.
cur_dir = op.dirname(op.realpath(__file__))
database_path = op.join(cur_dir, app.config["DATABASE_FILE"])
if not os.path.exists(database_path):
    build_sample_db()

if __name__ == "__main__":
    # Start app
    app.run(debug=True)
