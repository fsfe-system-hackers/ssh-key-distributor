# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
---

# - name: Debugging...
#   debug:
#     msg: "user={{ item.value.user }}, authfile={{ item.value.authfile }}, keyfile={{ lookup('file', item.value.keystringfile) }} "

- name: Get stats of authorized keys file for {{ item.value.user }}
  stat:
    path: "{{ item.value.authfile }}"
  register: st

- name: Backup old authorized keys file (non-PVE hosts) for {{ item.value.user }}
  copy:
    remote_src: yes
    src: "{{ item.value.authfile }}"
    dest: "{{ item.value.authfile }}.{{ ansible_date_time.iso8601_basic_short }}.bak"
    owner: "{{ st.stat.pw_name }}"
    group: "{{ st.stat.gr_name }}"
    mode: "{{ st.stat.mode }}"
  changed_when: false
  when:
    - '"/etc/pve" not in item.value.authfile'
    - st.stat.exists

- name: Backup old authorized keys file (ignore errors for PVE hosts) for {{ item.value.user }}
  # https://github.com/ansible/ansible/pull/70722 probably has to be merged into stable ansible versions first
  copy:
    remote_src: yes
    src: "{{ item.value.authfile }}"
    dest: "{{ item.value.authfile }}.{{ ansible_date_time.iso8601_basic_short }}.bak"
    owner: "{{ st.stat.pw_name }}"
    group: "{{ st.stat.gr_name }}"
    mode: "{{ st.stat.mode }}"
  changed_when: false
  ignore_errors: yes
  when:
    - '"/etc/pve" in item.value.authfile'
    - st.stat.exists

- name: Provide new public keys for {{ item.value.user }}
  block:
    - name: Provide new public keys for {{ item.value.user }}
      copy:
        src: "{{ item.value.keystringfile }}"
        dest: "{{ item.value.authfile }}"
        owner: "{{ st.stat.pw_name }}"
        group: "{{ st.stat.gr_name }}"
        mode: "{{ st.stat.mode }}"
  rescue:
    - name: Create new public keys file for {{ item.value.user }}
      copy:
        src: "{{ item.value.keystringfile }}"
        dest: "{{ item.value.authfile }}"
        owner: "{{ item.value.user }}"
        group: "{{ item.value.user }}"
        mode: 0600
