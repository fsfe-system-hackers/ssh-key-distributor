<!--
SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/ssh-key-distributor/00_README)
[![REUSE
status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/ssh-key-distributor)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/ssh-key-distributor) [![Build Status](https://drone.fsfe.org/api/badges/fsfe-system-hackers/ssh-key-distributor/status.svg)](https://drone.fsfe.org/fsfe-system-hackers/ssh-key-distributor)

# SSH Key Distributor

## Goal

Providing a simple way to ensure the presence (or absence) of public SSH keys on a
set of systems (user-host-combinations) according to profiles.

## Usage

If you want to run this directly using Python or develop the tool further, first
execute the following commands to get started with
[pipenv](https://pipenv.pypa.io/en/latest/). Make sure you're using at least
Python 3.9

```bash
pip install --user pipenv
pipenv install
```

then you should initialise the `inventory` and `ssh-data` submodules where all
the FSFE-specific data is stored.

```bash
git submodule update --init --remote inventory
git submodule update --init --remote ssh-data
```

finally run to start the UI:

```bash
pipenv run python admin.py
```

and open [http://localhost:5000/admin](http://localhost:5000/admin). There
you'll find instructions on how to proceed. It boils down to the following:

1. Configure keys and the systems to which they have access. This project uses
   the following concept to ease configuration:

   **Systems** are user-host-combinations, e.g. `exampleuser@exampleserver.com`.
   For each system the location of its authfile needs to be specified, e.g.
   `/home/exampleuser/.ssh/authfile`

   **Profiles** are simply a utility. They allow the user to specify which keys
   have access to which system in a more straightforward way than adding every
   key to every system individually. One can for example specify an `admin`
   group which has access to all root accounts on all hosts. Then adding a new
   key that is supposed to have this level of access can just be added to the
   `admin` profile.

   **Keys** are simply public SSH keys. They consist of a keytype (e.g.
   `ed25519`), the keystring, a comment and an option.

2. Once everything is configured correctly, you can generate the
   variables that the Ansible playbook at the root of this project needs in
   order to carry out its function, i.e. updating all the `~/.ssh/authorized_keys`
   files on all hosts. Generate the needed Ansible configuration files by
   clicking the button labelled `Generate Ansible Config`.
3. At this point, All that is left to do is to deploy all the keys via a [slim
   ansible playbook](playbook.yaml) using the command below:
   ```bash
   ansible-playbook playbook.yml
   ```

## Architecture

### UI

A simple CRUD interface based on [Flask
Admin](https://github.com/flask-admin/flask-admin) based on [data
model](admin/models.py) managed in [SQLAlchemy](https://www.sqlalchemy.org/) (a
powerful [ORM](https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping)
implementation)

### Database

A simple data model with three tables (`key`, `profile`, `system`) and two
association tables (`at_key_profile`, `at_profile_system`).

### Deployment

A [pre-deployment python script](admin/ansible.py) which creates and [the
correct variables for each host](host_vars/) and the contents of the
[keystringfiles directory](ssh-data/keystringfiles). This script is called from
the homepage of the UI, i.e. `/admin` by clicking the aforementioned button.
